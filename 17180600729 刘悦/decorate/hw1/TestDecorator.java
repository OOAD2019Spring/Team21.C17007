package HW1;

public class TestDecorator {

	public static void main(String[] args) {
		Icecream icecream=new SimpleIcecream();
		icecream=new NuttyDecorator(icecream);
		icecream=new HoneyDecorator(icecream);
		icecream=new NuttyDecorator(icecream);
		System.out.println(icecream.makeIcecream());
	}

}
