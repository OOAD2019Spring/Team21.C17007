package HW1;

public class HoneyDecorator extends IcecreamDecorator{
	public HoneyDecorator(Icecream specialIcecream) {
		super(specialIcecream);
	}
	public String makeIcecream() {
		return specialcecream.makeIcecream()+this.addHoney();
	}
	private String addHoney() {
		return ",Honey";
	}
}
