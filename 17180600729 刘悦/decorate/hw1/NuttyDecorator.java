package HW1;

public class NuttyDecorator extends IcecreamDecorator{
	public NuttyDecorator(Icecream specialIcecream) {
		super(specialIcecream);
	}
	public String makeIcecream() {
		return specialcecream.makeIcecream()+this.addNuts();
	}	
	private String addNuts() {
		return ",Nuts";
	}
}
