package HW1;

public abstract class IcecreamDecorator implements Icecream{
	protected Icecream specialcecream;
	public IcecreamDecorator(Icecream specialIcecream) {
		this.specialcecream=specialIcecream;
	}
	public abstract String makeIcecream();
}
